/*
 * Copyright (c) 2018. Vlad Kalyuzhnyu <vladkalyuzhnyu@gmail.com>
 */

@file:Suppress("unused")

package io.androidovshchik.project

const val NOISY_CHANNEL_ID = "noisy_channel"
const val QUITE_CHANNEL_ID = "quite_channel"

const val NUMBERS = "0123456789"
const val UPPER_LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
const val LOWER_LETTERS = "abcdefghijklmnopqrstuvwxyz"
const val LETTERS = UPPER_LETTERS + LOWER_LETTERS
const val CHARS = NUMBERS + LETTERS