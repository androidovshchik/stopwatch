/*
 * Copyright (c) 2018. Vlad Kalyuzhnyu <vladkalyuzhnyu@gmail.com>
 */

@file:Suppress("unused")

package io.androidovshchik.project.utils

import android.net.Uri

fun String.phone2Uri(): Uri {
    var uri = ""
    if (!startsWith("tel:")) {
        uri += "tel:"
    }
    val chars = toCharArray()
    for (c in chars) {
        uri += when (c) {
            '#' -> Uri.encode("#")
            else -> c
        }
    }
    return Uri.parse(uri)
}