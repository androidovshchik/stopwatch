/*
 * Copyright (c) 2018. Vlad Kalyuzhnyu <vladkalyuzhnyu@gmail.com>
 */

package io.androidovshchik.project

import android.os.Bundle
import io.androidovshchik.project.base.BaseV7Activity
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import java.util.concurrent.TimeUnit

class MainActivity : BaseV7Activity() {

    private var time = 0L

    private var counter = 0

    // needed to fix error of diff 21/20 for 3x speed
    private var counter3 = 0

    private var speed = 20

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        start.setOnClickListener { _ ->
            disposable.clear()
            disposable.add(Observable.interval(0, 50, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
                .subscribe {
                    counter++
                    if (speed == 7) {
                        counter3++
                        if (counter3 >= 20) {
                            counter3 -= 20
                            counter++
                        }
                    }
                    if (counter >= speed) {
                        counter = 0
                        time += 1000L
                        displayTime(time)
                    } else {
                        displayTime(time + Math.round(1000f * counter / speed))
                    }
                })
        }
        stop.setOnClickListener {
            disposable.clear()
        }
        reset.setOnClickListener {
            disposable.clear()
            time = 0L
            displayTime(time)
        }
        fast5.setOnClickListener {
            updateSpeed(4)
        }
        fast4.setOnClickListener {
            updateSpeed(5)
        }
        fast3.setOnClickListener {
            updateSpeed(7)
        }
        fast2.setOnClickListener {
            updateSpeed(10)
        }
        normal1.setOnClickListener {
            updateSpeed(20)
        }
        slow2.setOnClickListener {
            updateSpeed(40)
        }
        slow3.setOnClickListener {
            updateSpeed(60)
        }
        slow4.setOnClickListener {
            updateSpeed(80)
        }
        slow5.setOnClickListener {
            updateSpeed(100)
        }
        displayTime(time)
        updateSpeed(20)
    }

    private fun displayTime(time: Long) {
        val h = TimeUnit.MILLISECONDS.toHours(time)
        val m = TimeUnit.MILLISECONDS.toMinutes(time)
        val s = TimeUnit.MILLISECONDS.toSeconds(time)
        time_display.text = String.format(Locale.ENGLISH, "%d:%02d:%02d.%03d", h,
            m - TimeUnit.HOURS.toMinutes(h), s - TimeUnit.MINUTES.toSeconds(m),
            time - TimeUnit.SECONDS.toMillis(s))
    }

    private fun updateSpeed(speed: Int) {
        counter = Math.round(1f * counter * speed / this.speed)
        this.speed = speed
        current_speed.text = if (speed <= 20) {
            "${Math.round(20f / speed)}x"
        } else {
            "${String.format(Locale.ENGLISH, "%.2f", 20f / speed)
                .replace("\\.?0*$".toRegex(), "")}x"
        }
    }
}
